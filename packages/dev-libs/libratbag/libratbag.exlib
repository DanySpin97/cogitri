# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=libratbag tag=v${PV} ] python [ blacklist=2 multibuild=false ]
require meson

export_exlib_phases src_prepare

SUMMARY="A library to configure gaming mice "

LICENCES="MIT"
SLOT="0"

MYOPTIONS="
    ( providers: elogind systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-lang/python:*[>=3]
        dev-lang/swig
        dev-libs/glib:2
        x11-libs/libevdev
        providers:elogind? ( sys-auth/elogind[>=227] )
        providers:systemd? ( sys-apps/systemd[>=227] )
    test:
        app-admin/chrpath
        dev-libs/check[>=0.9.10]
        dev-python/lxml[python_abis:*(-)?]
        dev-util/valgrind
        gnome-bindings/pygobject:3[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-build-support-building-with-elogind.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Dudev-dir=/usr/$(exhost --target)/lib/udev
    -Dsystemd-unit-dir=/usr/$(exhost --target)/lib/systemd/system
    -Denable-documentation=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    providers:elogind
    providers:systemd
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=true -Dtests=false'
)

libratbag_src_prepare() {
    meson_src_prepare

    # Replace generic python3 shebang with python$(python_get_abi)
    edo find ./ -name "*.py" -exec sed "s/python3/python$(python_get_abi)/" -i {} \;
    edo find ./tools -name "*.in" -exec sed "s/python3/python$(python_get_abi)/" -i {} \;
}

