# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'synergy-1.8.8.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require cmake [ api=2 ] github [ user=symless tag=v${PV}-stable project=synergy-core ]
require gtk-icon-cache
require qmake [ slot=5 ]

SUMMARY="Share one mouse and keyboard between multiple computers"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gui [[ description = [ Build Synergy's GUI ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        net-misc/curl
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXtst
        gui? (
            net-dns/avahi[dns_sd]
            x11-libs/qtbase:5
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

# Restrict tests because they're broken and upstream currently
# is too busy to fix them.
RESTRICT=test

WORK="${WORKBASE}/${PN}-core-${PV}-stable/"
CMAKE_SOURCE="${WORKBASE}/${PN}-core-${PV}-stable/"
EQMAKE_SOURCES="gui.pro"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/synergy-1.8.1-internal-gmock-gtest.patch
    "${FILES}"/synergy-1.8.8-gtest.patch
)

src_configure() {
    cmake_src_configure

    if optionq gui; then
        edo pushd src/gui
        eqmake
        edo popd
    fi
}

src_compile() {
    emake

    if optionq gui; then
        edo pushd src/gui
        # dns_sd.h isn't in /usr/include, so we have to include it manually
        edo sed -i '/INCPATH/s/$/ -I\/usr\/include\/avahi-compat-libdns_sd\//' Makefile
        emake
        edo popd
    fi
}

src_install() {
    dobin bin/${PN}{c,s} bin/syntool

    if optionq gui; then
        dobin bin/${PN}
        insinto /usr/share/icons/hicolor/256x256/apps
        doins res/${PN}.ico
        insinto /usr/share/applications/
        hereins q${PN}.desktop <<EOF
[Desktop Entry]
Name=Synergy
Comment=Share your keyboard and mouse over a network
Exec=synergy
Icon=/usr/share/icons/hicolor/256x256/apps/synergy.ico
Type=Application
Categories=Utility;
EOF
    fi

    insinto /etc
    newins doc/synergy.conf.example synergy.conf
    newman doc/${PN}c.man ${PN}c.1
    newman doc/${PN}s.man ${PN}s.1
    dodoc README doc/synergy.conf.example* ChangeLog
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
}

pkg_preinst() {
    gtk-icon-cache_pkg_preinst
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
}

