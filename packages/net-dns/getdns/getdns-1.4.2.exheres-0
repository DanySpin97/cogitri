# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A modern asynchronous DNS API"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"

HOMEPAGE="https://getdnsapi.net"
DOWNLOADS="https://getdnsapi.net/dist/${PNV}.tar.gz"

MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        net-dns/libidn2
        net-dns/unbound
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.0.2] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dsa
    --enable-ecdsa
    --enable-gost
    --with-piddir=/run
    --with-ssl=/usr/$(exhost --target)
)

# Require net access
RESTRICT="test"

